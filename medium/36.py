# https://leetcode.com/problems/valid-sudoku/
from typing import List
import numpy as np


class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        s = np.ones(81, dtype=np.int8).reshape(9, 9) * -1
        for i in range(9):
            for j in range(9):
                el = board[i][j]
                if el != ".":
                    s[i][j] = int(el)

        # check rows/cols
        for i in range(9):
            row = s[i, :]
            if not self._is_valid(row):
                return False

            col = s[:, i]
            if not self._is_valid(col):
                return False

        # check squares
        squares = [
            s[0:3, 0:3], s[0:3, 3:6], s[0:3, 6:9],
            s[3:6, 0:3], s[3:6, 3:6], s[3:6, 6:9],
            s[6:9, 0:3], s[6:9, 3:6], s[6:9, 6:9],
        ]
        for square in squares:
            if not self._is_valid(square):
                return False

        return True

    def _is_valid(self, xs: np.ndarray) -> bool:
        filtered = xs[np.where(xs != -1)]
        return len(filtered) == len(set(filtered))


print(Solution().isValidSudoku([["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6"],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[".",".",".",".","8",".",".","7","9"]]))