# https://leetcode.com/problems/longest-palindromic-substring/


# time: O(n^2)
# space: O(n)
class Solution:
    def longestPalindrome(self, s: str) -> str:
        s_length = len(s)
        best_length = 0
        best_s = ""

        # odd size palindrome (min size 1)
        for mid in range(s_length):
            distance = 0
            while (mid - distance) >= 0 and (mid + distance) < s_length:
                if s[mid - distance] != s[mid + distance]:
                    break

                palindrome_length = distance * 2 + 1
                if palindrome_length > best_length:
                    best_length = palindrome_length
                    best_s = s[mid - distance:mid + distance + 1]

                distance += 1

        # even size palindrome (min size 2)
        for mid in range(s_length):
            distance = 1
            while (mid - distance + 1) >= 0 and (mid + distance) < s_length:
                if s[mid - distance + 1] != s[mid + distance]:
                    break

                palindrome_length = distance * 2
                if palindrome_length > best_length:
                    best_length = palindrome_length
                    best_s = s[mid - distance + 1:mid + distance + 1]

                distance += 1

        return best_s


print(Solution().longestPalindrome("babad") == "bab")
print(Solution().longestPalindrome("cbbd") == "bb")
