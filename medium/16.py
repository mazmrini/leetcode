# https://leetcode.com/problems/3sum-closest/
from typing import List


# time: O(n^2)
# space: O(1)
class Solution:
    def threeSumClosest(self, nums: List[int], target: int) -> int:
        nums = list(sorted(nums))
        closest = sum(nums[:3])
        if closest == target:
            return target

        for i in range(len(nums) - 2):
            num = nums[i]
            left, right = i + 1, len(nums) - 1

            while left < right:
                left_num = nums[left]
                right_num = nums[right]
                total = num + left_num + right_num
                if total == target:
                    return target
                elif total < target:
                    left += 1
                else:
                    right -= 1

                if abs(target - total) < abs(target - closest):
                    closest = total

        return closest


print(Solution().threeSumClosest([-1, 2, 1, -4], 1) == 2)
