# https://leetcode.com/problems/generate-parentheses/
from typing import List, Union


class N:
    def __init__(self, val: str, left: 'N' = None, right: 'N' = None) -> None:
        self.val = val
        self.left = left
        self.right = right

    def __repr__(self) -> str:
        return self.val


# Could be faster using some kind of memory when traversing the tree
class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        root = N("(", self._left(n - 1, n), self._right(n - 1, n))
        res = []
        self._read_left(res, root.val, root.left)
        self._read_right(res, root.val, root.right)

        return res

    def _read_left(self, res: List[str], s: str, n: N) -> None:
        if n is None:
            return

        self._read_left(res, s + n.val, n.left)
        self._read_right(res, s + n.val, n.right)

    def _read_right(self, res: List[str], s: str, n: N) -> None:
        if n is None:
            return

        if n.left is None and n.right is None:
            res.append(s + n.val)
            return

        self._read_left(res, s + n.val, n.left)
        self._read_right(res, s + n.val, n.right)

    def _left(self, n_open: int, n_close: int) -> Union[N, None]:
        if n_open == 0:
            return None

        return N("(", self._left(n_open - 1, n_close), self._right(n_open - 1, n_close))

    def _right(self, n_open: int, n_close: int) -> Union[N, None]:
        if n_close <= n_open:
            return None

        return N(")", self._left(n_open, n_close - 1), self._right(n_open, n_close - 1))


print(Solution().generateParenthesis(3) == ["((()))", "(()())", "(())()", "()(())", "()()()"])
print(Solution().generateParenthesis(2) == ["(())", "()()"])
print(Solution().generateParenthesis(1) == ["()"])
