# https://leetcode.com/problems/add-two-numbers/
import math


# Definition for singly-linked list.
class ListNode:
    def __init__(self, x: int) -> None:
        self.val = x
        self.next = None

    def __eq__(self, other: 'ListNode') -> bool:
        has_same_val = self.val == other.val
        if self.next is None or other.next is None:
            if self.next is None and other.next is None:
                return has_same_val

            return False

        has_same_next = self.next == other.next

        return has_same_next and has_same_val


# time: O(k) where k is the longest ListNode
# space: O(k+1)
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        total = self.__as_number(l1) + self.__as_number(l2)

        return self.__as_list_node(total)

    def __as_number(self, l: ListNode) -> int:
        exponent = 0
        total = 0
        current = l
        while current is not None:
            total += current.val * 10**exponent

            exponent += 1
            current = current.next

        return total

    def __as_list_node(self, number: int) -> ListNode:
        as_string = str(number)[::-1]
        root = ListNode(int(as_string[0]))
        current = root

        for i in range(1, len(as_string)):
            current.next = ListNode(int(as_string[i]))
            current = current.next

        return root


# Test 1
firstValue = 342
firstNode = ListNode(2)
firstNode.next = ListNode(4)
firstNode.next.next = ListNode(3)

secondValue = 465
secondNode = ListNode(5)
secondNode.next = ListNode(6)
secondNode.next.next = ListNode(4)

answerNode = ListNode(7)
answerNode.next = ListNode(0)
answerNode.next.next = ListNode(8)

print(Solution().addTwoNumbers(firstNode, secondNode) == answerNode)

# Test 2
firstValue = 942
firstNode = ListNode(2)
firstNode.next = ListNode(4)
firstNode.next.next = ListNode(9)

secondValue = 465
secondNode = ListNode(5)
secondNode.next = ListNode(6)
secondNode.next.next = ListNode(4)

answerNode = ListNode(7)
answerNode.next = ListNode(0)
answerNode.next.next = ListNode(4)
answerNode.next.next.next = ListNode(1)

print(Solution().addTwoNumbers(firstNode, secondNode) == answerNode)
