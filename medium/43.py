# https://leetcode.com/problems/multiply-strings/

class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        if num1 == "0" or num2 == "0":
            return "0"

        result = self._to_int(num1) * self._to_int(num2)

        return str(result)

    def _to_int(self, n: str) -> int:
        ns = {str(i): i for i in range(10)}
        res = 0
        for i in range(len(n), 0, -1):
            res += ns[n[len(n) - i]] * 10**(i - 1)

        return res


print(Solution().multiply("2", "3") == "6")
print(Solution().multiply("123", "456") == "56088")
print(Solution().multiply("25", "25") == "625")
print(Solution().multiply("20", "0") == "0")
print(Solution().multiply("0", "0") == "0")
