# https://leetcode.com/problems/integer-to-roman/
from typing import List, Tuple


# time: O(1)
# space: O(length num in roman)
class Solution:
    NUMBERS_WITH_ROMAN: List[Tuple[int, str]] = [
        (1000, "M"),
        (900, "CM"),
        (500, "D"),
        (400, "CD"),
        (100, "C"),
        (90, "XC"),
        (50, "L"),
        (40, "XL"),
        (10, "X"),
        (9, "IX"),
        (5, "V"),
        (4, "IV"),
        (1, "I")
    ]

    def intToRoman(self, num: int) -> str:
        result = ""
        left = num
        for divisor, str_equiv in Solution.NUMBERS_WITH_ROMAN:
            nb_times = left // divisor
            left -= divisor * nb_times
            result += str_equiv * nb_times

        return result


print(Solution().intToRoman(3) == "III")
print(Solution().intToRoman(4) == "IV")
print(Solution().intToRoman(9) == "IX")
print(Solution().intToRoman(58) == "LVIII")
print(Solution().intToRoman(1994) == "MCMXCIV")
