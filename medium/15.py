# https://leetcode.com/problems/3sum/
from typing import List


# time: O(n^2)
# space: O(n)
class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums = list(sorted(nums))
        result = []
        visited = set()

        for i in range(len(nums) - 2):
            num = nums[i]
            if i > 0 and num == nums[i - 1]:
                continue

            left, right = i + 1, len(nums) - 1
            left_num = nums[left]
            if (num, left_num) in visited:
                continue

            visited.add((num, left_num))
            while left < right:
                left_num = nums[left]
                right_num = nums[right]
                total = num + left_num + right_num
                if total == 0:
                    response = (num, left_num, right_num)
                    if response not in visited:
                        visited.add(response)
                        result.append(response)
                    left += 1
                    right -= 1
                elif total < 0:
                    left += 1
                else:
                    right -= 1

            if num >= 0:
                break

        return result
