# https://leetcode.com/problems/remove-nth-node-from-end-of-list/
from typing import List


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        length = 1
        tmp = head
        prev_tail = head
        while tmp.next is not None:
            length += 1
            prev_tail = tmp
            tmp = tmp.next

        if n == 1:
            prev_tail.next = None
            return head

        if n == length:
            return head.next

        current_prev = head
        current = 1
        goal = length - n
        while current < goal:
            current += 1
            current_prev = current_prev.next

        # next is the one we want to remove
        current_prev.next = current_prev.next.next

        return head


def to_list_node(xs: List[int]) -> ListNode:
    head = ListNode(xs[0])
    tmp = head
    for x in xs[1:]:
        tmp.next = ListNode(x)
        tmp = tmp.next

    return head


def to_list(h: ListNode) -> List[int]:
    tmp = h
    result = []
    while tmp is not None:
        result.append(tmp.val)
        tmp = tmp.next

    return result


h = to_list_node([1, 2, 3, 4, 5, 6, 7])
print(to_list(h) == [1, 2, 3, 4, 5, 6, 7])
print()

h = to_list_node([1, 2, 3, 4, 5, 6, 7])
actual = Solution().removeNthFromEnd(h, 3)
expected = [1, 2, 3, 4, 6, 7]
print(to_list(actual) == expected)

h = to_list_node([20, 10, 5])
actual = Solution().removeNthFromEnd(h, 1)
expected = [20, 10]
print(to_list(actual) == expected)

h = to_list_node([1, 2, 3, 4, 5, 6, 7])
actual = Solution().removeNthFromEnd(h, 6)
expected = [1, 3, 4, 5, 6, 7]
print(to_list(actual) == expected)

h = to_list_node([1, 2, 3, 4, 5, 6, 7])
actual = Solution().removeNthFromEnd(h, 7)
expected = [2, 3, 4, 5, 6, 7]
print(to_list(actual) == expected)
