# https://leetcode.com/problems/zigzag-conversion/


# time: O(n)
# space: O(n)
class Solution:
    def convert(self, s: str, num_rows: int) -> str:
        if num_rows == 1:
            return s

        zig_zag = ["" for _ in range(len(s))]

        row = 0
        step = 0
        for char in s:
            if row == 0:
                step = 1
            elif row == num_rows - 1:
                step = -1

            zig_zag[row] += char
            row += step

        return "".join(zig_zag)


print(Solution().convert("PAYPALISHIRING", 1) == "PAYPALISHIRING")
print(Solution().convert("PAYPALISHIRING", 2) == "PYAIHRNAPLSIIG")
print(Solution().convert("PAYPALISHIRING", 3) == "PAHNAPLSIIGYIR")
print(Solution().convert("PAYPALISHIRING", 4) == "PINALSIGYAHRPI")
print(Solution().convert("PAYPALISHIRING", 5) == "PHASIYIRPLIGAN")
