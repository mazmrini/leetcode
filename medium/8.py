# https://leetcode.com/problems/string-to-integer-atoi/


# time: O(n)
# space: O(n)
class Solution:
    INT_MIN = -2**31
    INT_MAX = 2**31 - 1

    def myAtoi(self, text: str) -> int:
        number_as_str = self.__extract_number_as_string(text)
        if len(number_as_str) == 0:
            return 0

        negative_multiplier = 1
        if number_as_str[0] == "-":
            number_as_str = number_as_str[1:]
            negative_multiplier = -1

        total = 0
        for i, number in enumerate(number_as_str[::-1]):
            next_number = int(number) * 10**i
            if negative_multiplier == 1:
                if next_number > (Solution.INT_MAX - total):
                    return Solution.INT_MAX
            else:
                if next_number > (-1 * Solution.INT_MIN - total):
                    return Solution.INT_MIN

            total += next_number

        return total * negative_multiplier

    def __extract_number_as_string(self, text: str) -> str:
        if len(text) == 1:
            if text.isnumeric():
                return text
            else:
                return ""

        first_legal_char_index = self.__first_legal_char_index(text)
        if first_legal_char_index < 0:
            return ""

        number_as_str = text[first_legal_char_index]
        for i in range(first_legal_char_index + 1, len(text)):
            char = text[i]
            if char.isnumeric():
                number_as_str += char
            else:
                break

        return number_as_str

    def __first_legal_char_index(self, text: str) -> int:
        for i in range(len(text) - 1):
            char = text[i]
            if char == " ":
                continue
            elif char.isnumeric():
                return i
            elif char == "-" and text[i + 1].isnumeric():
                return i
            elif char == "+" and text[i + 1].isnumeric():
                return i + 1
            else:
                return -1

        return -1


print(Solution().myAtoi("-") == 0)
print(Solution().myAtoi("+") == 0)
print(Solution().myAtoi("5") == 5)
print(Solution().myAtoi("42") == 42)
print(Solution().myAtoi("   -42") == -42)
print(Solution().myAtoi("4193 with words") == 4193)
print(Solution().myAtoi("    +4193 with words") == 4193)
print(Solution().myAtoi("words and 987") == 0)
print(Solution().myAtoi("   -abc") == 0)
print(Solution().myAtoi("-91283472332") == Solution.INT_MIN)
print(Solution().myAtoi("991283472332") == Solution.INT_MAX)
