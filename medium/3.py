# https://leetcode.com/problems/longest-substring-without-repeating-characters/


# time: O(n)
# space: O(n)
class Solution:
    def lengthOfLongestSubstring(self, text: str) -> int:
        text_len = len(text)
        longest_substring_length = 0
        current_substring_length = 0
        start_index = 0
        i = 0
        chars_found = set()
        while i != text_len:
            char_at_i = text[i]
            if char_at_i not in chars_found:
                chars_found.add(char_at_i)
                current_substring_length += 1
                i += 1
                if current_substring_length > longest_substring_length:
                    longest_substring_length = current_substring_length
            else:
                chars_found.remove(text[start_index])
                current_substring_length -= 1
                start_index += 1

        return longest_substring_length


print(Solution().lengthOfLongestSubstring("abcabcbb") == 3)
print(Solution().lengthOfLongestSubstring("bbbbb") == 1)
print(Solution().lengthOfLongestSubstring("pwwkew") == 3)
