# https://leetcode.com/problems/divide-two-integers/


class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        if divisor == 1:
            return dividend
        if divisor == -1:
            return -dividend

        n = dividend if dividend > 0 else 0 - dividend
        d = divisor if divisor > 0 else 0 - divisor

        r = 0
        t = 0
        while t <= (n - d):
            r += 1
            t += d

        if dividend < 0 and divisor < 0:
            return r

        if dividend < 0 or divisor < 0:
            return 0 - r

        return r


print(Solution().divide(10, 3) == 3)
print(Solution().divide(7, -3) == -2)
print(Solution().divide(-18, 42) == 0)
print(Solution().divide(-99, 12) == -8)
print(Solution().divide(-99, -99) == 1)
print(Solution().divide(-1029201, 1) == -1029201)
print(Solution().divide(-1029201, -1) == 1029201)

