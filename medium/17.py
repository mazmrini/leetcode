# https://leetcode.com/problems/letter-combinations-of-a-phone-number/
from typing import List


class Solution:
    NUMBERS_TO_LETTERS = {
        "2": ["a", "b", "c"],
        "3": ["d", "e", "f"],
        "4": ["g", "h", "i"],
        "5": ["j", "k", "l"],
        "6": ["m", "n", "o"],
        "7": ["p", "q", "r", "s"],
        "8": ["t", "u", "v"],
        "9": ["w", "x", "y", "z"]
    }

    def letterCombinations(self, digits: str) -> List[str]:
        if digits == "":
            return []

        return self.__letter_combinations(digits)
    
    def __letter_combinations(self, digits: str) -> List[str]:
        if len(digits) == 1:
            return Solution.NUMBERS_TO_LETTERS[digits]

        digit = digits[0]
        partial_solutions = self.__letter_combinations(digits[1:])
        result = []
        for letter in Solution.NUMBERS_TO_LETTERS[digit]:
            for solution in partial_solutions:
                result.append(letter + solution)

        return result


print(Solution().letterCombinations("2") == ["a", "b", "c"])
print(Solution().letterCombinations("23") == ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"])
print(Solution().letterCombinations("") == [])
