# https://leetcode.com/problems/container-with-most-water/
from typing import List


# time: O(n)
# space: O(n)
class Solution:
    def maxArea(self, heights: List[int]) -> int:
        max_area = 0
        left = 0
        right = len(heights) - 1
        while left != right:
            left_height, right_height = heights[left], heights[right]
            area = min(left_height, right_height) * (right - left)
            max_area = max(area, max_area)

            if left_height < right_height:
                left += 1
            else:
                right -= 1

        return max_area


print(Solution().maxArea([2, 3, 4, 5, 18, 17, 6]) == 17)
print(Solution().maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]) == 49)
