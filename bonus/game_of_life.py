# Rule 1: Any live cell with fewer than two live neighbours dies, as if by underpopulation.
# Rule 2: Any live cell with two or three live neighbours lives on to the next generation.
# Rule 3: Any live cell with more than three live neighbours dies, as if by overpopulation.
# Rule 4: Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

# 1. Create a 2D board
# 2. Populate the board
# 3. Be able to display the board in the console
# 4. Be able to play iteration of the game following the rules
from typing import Tuple, List


class Cell:
    def __init__(self, loc: Tuple[int, int], is_alive: bool):
        self.loc = loc
        self.is_alive = is_alive
        self.neighbors: List['Cell'] = []

    def live_neighbors(self) -> List['Cell']:
        return [neighbor for neighbor in self.neighbors if neighbor.is_alive]


class Game:
    def __init__(self, nb_rows: int, nb_cols: int) -> None:
        self.nb_rows = nb_rows
        self.nb_cols = nb_cols
        self.board: List[List[Cell]] = [
            [Cell((x, y), False) for y in range(nb_cols)]
            for x in range(nb_rows)
        ]
        self.cells = []
        for row in self.board:
            self.cells += row

        self._link_neighbors()

    def populate(self, cells: List[Cell]) -> None:
        for cell in cells:
            x, y = cell.loc
            self.board[x][y].is_alive = cell.is_alive

    def play(self) -> None:
        changes = []
        for cell in self.cells:
            nb_live_neighbors = len(cell.live_neighbors())
            if cell.is_alive:
                if nb_live_neighbors < 2 or nb_live_neighbors > 3:
                    changes.append(Cell(cell.loc, False))
            else:
                if nb_live_neighbors == 3:
                    changes.append(Cell(cell.loc, True))

        self.populate(changes)

    def _link_neighbors(self) -> None:
        for cell in self.cells:
            x, y = cell.loc
            neighbor_candidates = [(x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
                                   (x, y - 1), (x, y + 1),
                                   (x + 1, y - 1), (x + 1, y), (x + 1, y + 1)]
            for candidate_location in neighbor_candidates:
                x, y = candidate_location
                if self._is_in_board(x, y):
                    neighbor = self.board[x][y]
                    cell.neighbors.append(neighbor)

    def _is_in_board(self, x: int, y: int) -> bool:
        return 0 <= x < self.nb_rows and 0 <= y < self.nb_cols


class GameDisplayer:
    def show(self, game: Game) -> None:
        sep = "-" * (game.nb_cols * 4 + 1)
        res = sep + "\n"
        for row in game.board:
            print_me = [" + " if cell.is_alive else "   " for cell in row]
            res += (f"|{'|'.join(print_me)}|"
                    f"\n"
                    f"{sep}"
                    f"\n")

        print(res)


def main() -> None:
    displayer = GameDisplayer()

    game = Game(5, 8)
    game.populate([Cell((1, 2), True), Cell((2, 2), True), Cell((1, 3), True),
                   Cell((3, 5), True), Cell((4, 4), True), Cell((4, 5), True)])

    n = 10
    displayer.show(game)
    for i in range(n):
        game.play()
        print(f"#{i + 1}")
        displayer.show(game)


main()
