# https://leetcode.com/problems/reverse-integer/
import math


# time: O(n_chars)
# space: O(n_chars)
class Solution:
    def reverse(self, x: int) -> int:
        if x == 0:
            return 0

        total = x
        negative_multiplier = 1
        if x < 0:
            total *= -1
            negative_multiplier = -1

        answer = 0
        max_exponent = math.floor(math.log10(total))
        for exponent in range(max_exponent, -1, -1):
            exponent_multiplier = 10 ** exponent
            number = total // exponent_multiplier

            total -= number * exponent_multiplier
            answer += number * 10 ** (max_exponent - exponent)

        answer *= negative_multiplier

        return 0 if not -2 ** 31 <= answer <= 2 ** 31 - 1 else answer


print(Solution().reverse(0) == 0)
print(Solution().reverse(123) == 321)
print(Solution().reverse(-123) == -321)
