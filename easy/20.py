# https://leetcode.com/problems/valid-parentheses/


class Solution:
    def isValid(self, s: str) -> bool:
        match = {")": "(", "}": "{", "]": "["}
        stack = []
        for v in s:
            if v in ["(", "{", "["]:
                stack += [v]
            elif v in [")", "}", "]"]:
                if len(stack) > 0 and stack[-1] == match[v]:
                    stack = stack[:-1]
                else:
                    return False

        return len(stack) == 0


print(Solution().isValid("(({})[{{}}])()") == True)
print(Solution().isValid("()[]{}({})[({})]") == True)
print(Solution().isValid("({") == False)
print(Solution().isValid("({})[(])") == False)
print(Solution().isValid("") == True)
print(Solution().isValid("]") == False)
print(Solution().isValid("(){}}{") == False)
