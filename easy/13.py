# https://leetcode.com/problems/roman-to-integer/
from typing import Dict


# time: O(n)
# space: O(1)
class Solution:
    ROMAN_TO_INT: Dict[str, int] = {
         "M": 1000,
         "CM": 900,
         "D": 500,
         "CD": 400,
         "C": 100,
         "XC": 90,
         "L": 50,
         "XL": 40,
         "X": 10,
         "IX": 9,
         "V": 5,
         "IV": 4,
         "I": 1
    }

    def romanToInt(self, s: str) -> int:
        i = 0
        s_length = len(s)
        total = 0
        while i < s_length - 1:
            two_next_chars = s[i:i + 2]
            if two_next_chars in Solution.ROMAN_TO_INT:
                total += Solution.ROMAN_TO_INT[two_next_chars]
                i += 2
            else:
                total += Solution.ROMAN_TO_INT[s[i]]
                i += 1

        if i < s_length:
            total += Solution.ROMAN_TO_INT[s[i]]

        return total


print(Solution().romanToInt("III") == 3)
print(Solution().romanToInt("IV") == 4)
print(Solution().romanToInt("IX") == 9)
print(Solution().romanToInt("LVIII") == 58)
print(Solution().romanToInt("MCMXCIV") == 1994)
