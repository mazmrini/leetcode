# https://leetcode.com/problems/longest-common-prefix/
from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ""

        min_len_word = min(strs, key=lambda x: len(x))

        prefix = ""
        for i, char in enumerate(min_len_word):
            if all(word[i] == char for word in strs):
                prefix += char
            else:
                return prefix

        return prefix


print(Solution().longestCommonPrefix(["flower", "flow", "flight"]) == "fl")
print(Solution().longestCommonPrefix(["flowerflower"] * 40) == "flowerflower")
print(Solution().longestCommonPrefix(["dog", "racecar", "car"]) == "")
