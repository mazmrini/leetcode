# https://leetcode.com/problems/two-sum/
from typing import List


# time: O(n)
# space: O(n)
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        complements = set()

        for i, number in enumerate(nums):
            if number in complements:
                return [nums.index(target - number), i]
            else:
                complements.add(target - number)

        return []


print(Solution().twoSum([2, 7, 11, 15], 9) == [0, 1])
