# https://leetcode.com/problems/palindrome-number/


# time: O(n) where n is str length of x
# space: O(n)
class Solution:
    def isPalindrome(self, x: int) -> bool:
        x_as_str = str(x)

        return x_as_str == x_as_str[::-1]


print(Solution().isPalindrome(123454321) == True)
print(Solution().isPalindrome(98789) == True)
print(Solution().isPalindrome(2) == True)
print(Solution().isPalindrome(22) == True)
print(Solution().isPalindrome(345543) == True)
print(Solution().isPalindrome(1234) == False)
print(Solution().isPalindrome(871237) == False)
print(Solution().isPalindrome(-1234321) == False)
