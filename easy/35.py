# https://leetcode.com/problems/search-insert-position/
from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if target <= nums[0]:
            return 0
        if target > nums[-1]:
            return len(nums)

        l, r = 0, len(nums) - 1
        mid = (l + r) // 2
        while l <= r:
            mid = (l + r) // 2
            if target == nums[mid]:
                return mid
            elif target > nums[mid]:
                l = mid + 1
            elif target < nums[mid]:
                r = mid - 1

        if l > r:
            return l
        elif r > l:
            return r
        else:
            return mid


print(Solution().searchInsert([1, 3, 5], 4) == 2)
print(Solution().searchInsert([1, 3, 5], 1) == 0)
print(Solution().searchInsert([1, 3, 5, 6], 2) == 1)
print(Solution().searchInsert([1, 3, 5, 6], 5) == 2)
print(Solution().searchInsert([1, 3, 5, 6], 7) == 4)
print(Solution().searchInsert([1, 3], 2) == 1)
print(Solution().searchInsert([1, 3], 3) == 1)
print(Solution().searchInsert([1], 0) == 0)
