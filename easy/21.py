# https://leetcode.com/problems/merge-two-sorted-lists/


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 is None:
            return l2
        if l2 is None:
            return l1

        if l1.val <= l2.val:
            r = ListNode(l1.val)
            l1_c = l1.next
            l2_c = l2
        else:
            r = ListNode(l2.val)
            l1_c = l1
            l2_c = l2.next

        tmp = r
        while l1_c is not None and l2_c is not None:
            if l1_c.val <= l2_c.val:
                tmp.next = l1_c
                l1_c = l1_c.next
            else:
                tmp.next = l2_c
                l2_c = l2_c.next
            tmp = tmp.next

        if l1_c is not None:
            tmp.next = l1_c

        if l2_c is not None:
            tmp.next = l2_c

        return r
