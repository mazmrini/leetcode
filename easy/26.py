# https://leetcode.com/problems/remove-duplicates-from-sorted-array/
from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return len(nums)

        i = 0
        while i < len(nums) - 1:
            if nums[i] == nums[i+1]:
                nums.remove(nums[i])
            else:
                i += 1

        return len(nums)


nums = [1, 1, 2]
print(Solution().removeDuplicates(nums) == 2)
print(nums == [1, 2])
nums = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
print(Solution().removeDuplicates(nums) == 5)
print(nums == [0, 1, 2, 3, 4])
