# https://leetcode.com/problems/implement-strstr/


class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if haystack == needle:
            return 0

        if len(needle) > len(haystack):
            return -1

        if len(needle) == 0:
            return 0

        for i in range(0, len(haystack) - len(needle) + 1):
            if haystack[i:i+len(needle)] == needle:
                return i

        return -1


print(Solution().strStr("hello", "ll") == 2)
print(Solution().strStr("a", "a") == 0)
print(Solution().strStr("aaaaaa", "bbab") == -1)
print(Solution().strStr("aa", "bbab") == -1)
print(Solution().strStr("", "") == 0)
print(Solution().strStr("aaa", "") == 0)
print(Solution().strStr("abc", "c") == 2)
