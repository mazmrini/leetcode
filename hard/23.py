# https://leetcode.com/problems/merge-k-sorted-lists/
from typing import List


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeKLists(self, lists: List[ListNode]) -> ListNode:
        if len(lists) == 0:
            return None

        if len([1 for node in lists if node is not None]) == 0:
            return None

        nodes = []
        for l in lists:
            tmp = l
            while tmp is not None:
                nodes.append(tmp)
                tmp = tmp.next

        nodes = sorted(nodes, key=lambda n: n.val)
        nodes[-1].next = None
        for i, node in enumerate(nodes[:-1]):
            next_node = nodes[i + 1]
            node.next = next_node

        return nodes[0]


def to_list_node(xs: List[int]) -> ListNode:
    head = ListNode(xs[0])
    tmp = head
    for x in xs[1:]:
        tmp.next = ListNode(x)
        tmp = tmp.next

    return head


def to_list(h: ListNode) -> List[int]:
    tmp = h
    result = []
    while tmp is not None:
        result.append(tmp.val)
        tmp = tmp.next

    return result


h = to_list_node([1, 2, 3, 4, 5, 6, 7])
print(to_list(h) == [1, 2, 3, 4, 5, 6, 7])
print()

xs = [to_list_node([1, 2, 3, 6]), None, to_list_node([1, 3, 4]), to_list_node([6, 7])]
actual = Solution().mergeKLists(xs)
expected = [1, 1, 2, 3, 3, 4, 6, 6, 7]
print(to_list(actual) == expected)

actual = Solution().mergeKLists([])
print(actual is None)

actual = Solution().mergeKLists([None, None, None])
print(actual is None)

xs = [None, None, None, to_list_node([2, 3, 6]), None, None, None]
actual = Solution().mergeKLists(xs)
expected = [2, 3, 6]
print(to_list(actual) == expected)
