# https://leetcode.com/problems/substring-with-concatenation-of-all-words/
from typing import List


class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        word_length = len(words[0])
        substring_length = word_length * len(words)
        if substring_length > len(s):
            return []

        ws = dict()
        for word in words:
            if word not in ws:
                ws[word] = 0
            ws[word] += 1

        indexes = []
        for i in range(0, len(s) - substring_length + 1):
            substring = s[i:i+substring_length]
            tmp = ws.copy()
            for j in range(0, substring_length, word_length):
                subsubstring = substring[j:j+word_length]
                if subsubstring in tmp:
                    if tmp[subsubstring] == 1:
                        del tmp[subsubstring]
                    else:
                        tmp[subsubstring] -= 1
                else:
                    break

            if len(tmp) == 0:
                indexes.append(i)

        return indexes


print(Solution().findSubstring("barfoothefoobarman", ["foo", "bar"]) == [0, 9])
print(Solution().findSubstring("wordgoodgoodgoodbestword", ["word", "good", "best", "word"]) == [])
print(Solution().findSubstring("wordgoodgoodgoodbestword", ["word", "good", "best", "good"]) == [8])
print(Solution().findSubstring("barfoofoobarthefoobarman", ["bar", "foo", "the"]) == [6, 9, 12])
print(Solution().findSubstring("aaaaaaaaaaaaaa", ["aa", "aa"]) == list(range(0, 11)))
print(Solution().findSubstring("wordgoodbest", ["word", "good", "best", "word"]) == [])
