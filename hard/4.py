# https://leetcode.com/problems/median-of-two-sorted-arrays/
from typing import List

# time: O(log(m + n))
# try to improve to O(log(m + n))
# time: O((m + n) log(m + n))
# space: O(1)
class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        arr = nums1 + nums2
        arr = list(sorted(arr))

        mid = arr[len(arr) // 2]
        if len(arr) % 2 == 0:
            mid = (mid + arr[len(arr) // 2 - 1]) / 2

        return mid


print(Solution().findMedianSortedArrays([1, 3], [2]) == 2.0)
print(Solution().findMedianSortedArrays([1, 2], [3, 4]) == 2.5)
