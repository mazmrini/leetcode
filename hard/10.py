# https://leetcode.com/problems/regular-expression-matching/
from typing import Dict, Tuple


# time: O(n) ?
# space: O(n)
class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        return self.__is_next_matching(s, p, 0, 0, {})

    def __is_next_matching(self, s: str, p: str, s_index: int, p_index: int, memory: Dict[Tuple, bool]) -> bool:
        current_s = s[s_index:]
        current_p = p[p_index:]
        len_current_s = len(current_s)
        len_current_p = len(current_p)

        if (current_s, current_p) in memory:
            return memory[(current_s, current_p)]

        if len_current_s == 0:
            if current_p == "":
                return True
            elif len_current_p > 1 and current_p[1] == "*":
                res = self.__is_next_matching(s, p, s_index, p_index + 2, memory)
                memory[(current_s, current_p[2:])] = res

                return res
            else:
                return False

        if len_current_p == 0:
            return False

        if current_s[0] == current_p[0] or current_p[0] == ".":
            if len_current_p > 1:
                if current_p[1] == "*":
                    with_star = self.__is_next_matching(s, p, s_index + 1, p_index, memory)
                    memory[(current_s[1:], current_p)] = with_star

                    without_star = self.__is_next_matching(s, p, s_index, p_index + 2, memory)
                    memory[(current_s, current_p[2:])] = without_star

                    return with_star or without_star
                else:
                    res = self.__is_next_matching(s, p, s_index + 1, p_index + 1, memory)
                    memory[(current_s[1:], current_p[1:])] = res

                    return res
            else:
                res = self.__is_next_matching(s, p, s_index + 1, p_index + 1, memory)
                memory[(current_s[1:], current_p[1:])] = res

                return res
        elif len_current_p > 1:
            if current_p[1] == "*":
                res = self.__is_next_matching(s, p, s_index, p_index + 2, memory)
                memory[(current_s, current_p[2:])] = res

                return res

        return False


print(Solution().isMatch("aa", "a*") == True)
print(Solution().isMatch("aa", "a") == False)
print(Solution().isMatch("ab", ".*") == True)
print(Solution().isMatch("aab", "c*a*b") == True)
print(Solution().isMatch("mississippi", "mis*is*p*.") == False)
print(Solution().isMatch("mississippi", "mi*s*i.*") == True)
print(Solution().isMatch("mississippi", "mi*s*i.*p*.") == True)
print(Solution().isMatch("I love chocolate", "I love chocolate") == True)
print(Solution().isMatch("aaa", "a*a") == True)
print(Solution().isMatch("bbbba", ".*a*a*a*a*a") == True)
print(Solution().isMatch("", ".*") == True)
print(Solution().isMatch("", "") == True)
print(Solution().isMatch("aaa", "") == False)
print(Solution().isMatch("b", "aaa.") == False)
print(Solution().isMatch("", "c*c*") == True)
print(Solution().isMatch("a", ".*..a*") == False)
